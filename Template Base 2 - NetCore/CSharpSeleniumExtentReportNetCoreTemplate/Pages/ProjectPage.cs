using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Bases
{
    public class ProjectPage : PageBase
    {
        #region Mapping
        By gerenciarButton = By.XPath("//button[@class='btn btn-crowdtest mr-1'][(text()='Gerenciar')]");
        By projetoButton = By.XPath("//div[@id='selectProject-div']");
        By moduloButton = By.XPath("//div[@role='option']/span[text()='Módulo 1 - Testes Manuais']");
        By casoTesteButton = By.XPath("//div/span[(text()='Casos de Teste')]");
        By testCaseInfoArea = By.XPath("//mat-row/mat-cell[(text()='CT-001- Cadastrar Parte Envolvida')]");
        #endregion

        #region Actions
        public void ClicarEmGerenciar()
        {
            Click(gerenciarButton);
        }

        public void ClicarEmProjeto()
        {
            Click(projetoButton);
        }

        public void clicarEmModulo()
        {
            Click(moduloButton);
        }

        public void ClicarEmCasosDeTeste()
        {
            Click(casoTesteButton);
        }
        #endregion

        public string RetornaCaseTestExistente(){
            return GetText(testCaseInfoArea);
        }
    }
}
    
