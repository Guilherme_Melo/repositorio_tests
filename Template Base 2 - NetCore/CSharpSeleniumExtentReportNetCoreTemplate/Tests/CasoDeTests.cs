using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using CSharpSeleniumExtentReportNetCoreTemplate.Flows;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class CasoDeTests : TestBase
    {

        #region Pages and Flows Objects
        LoginPage loginPage;
        ProjectPage projectPage;
        LoginFlows loginFlows;
        #endregion

        [Test]
        public void RealizarLoginComSucesso()
        {
            loginFlows = new LoginFlows();
            projectPage = new ProjectPage();
            
            #region Parameters
            string usuario = "guilherme.melo@base2.com.br";
            string senha = "mastergame86314*";
            string validacao = "CT-001- Cadastrar Parte Envolvida";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            
            projectPage.ClicarEmGerenciar();
            projectPage.ClicarEmProjeto();
            projectPage.clicarEmModulo();
            projectPage.ClicarEmCasosDeTeste();

            Assert.AreEqual(validacao, projectPage.RetornaCaseTestExistente());

        }

    }
}
